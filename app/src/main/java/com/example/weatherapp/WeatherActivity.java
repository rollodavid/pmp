package com.example.weatherapp;

import static com.google.android.gms.location.LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.CancellationTokenSource;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

public class WeatherActivity extends AppCompatActivity {

    private FusedLocationProviderClient fusedLocationClient;
    String city;
    double lat = 0.0;
    double lon = 0.0;

    @SuppressLint({"MissingPermission", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        city = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        // Location
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (city.equals("here")) {
            updatePosition();
        } else if (city.equals("")) {
            Toast.makeText(this, "City name is empty", Toast.LENGTH_SHORT).show();
        } else {
            getWeather("https://api.openweathermap.org/data/2.5/weather?q=" + city); //GetWeatherByCityName(city);
        }

        // Refresh button
        Button btnRefresh = findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (city.equals("here")) {
                    updatePosition();
                } else if (city.equals("")) {
                    Toast.makeText(WeatherActivity.this, "City name is empty", Toast.LENGTH_SHORT).show();
                } else {
                    getWeather("https://api.openweathermap.org/data/2.5/weather?q=" + city);
                    Toast.makeText(WeatherActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getWeather(String url) {
        RequestQueue queue = Volley.newRequestQueue(this);

        String APIKey = "4e5deadbfffca4bca321598df873c692";
        String units = "metric";
        url = url + "&units=" + units + "&appid=" + APIKey;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {
                try {
                    //Name and country
                    TextView cityNameTv = findViewById(R.id.cityNameTv);
                    cityNameTv.setText(response.getString("name") + " (" + response.getJSONObject("sys").getString("country") + ")");

                    //Description
                    TextView descriptionTv = findViewById(R.id.descriptionTv);
                    JSONObject weather = response.getJSONArray("weather").getJSONObject(0);
                    String description = weather.getString("description");
                    descriptionTv.setText(description.substring(0,1).toUpperCase() + description.substring(1));

                    //Temp and pressure
                    TextView tempTv = findViewById(R.id.tempTv);
                    TextView pressureTv = findViewById(R.id.pressureTv);
                    JSONObject main = response.getJSONObject("main");
                    tempTv.setText(main.getString("temp") + " °C");
                    pressureTv.setText(main.getString("pressure") + " hPa");

                    //Wind speed and deg
                    TextView windTv = findViewById(R.id.windTv);
                    JSONObject wind = response.getJSONObject("wind");

                    String degString = "";
                    double deg = Double.parseDouble(wind.getString("deg"));

                    if (deg > 337.5 | deg <= 22.5) degString = "East ➡";
                    else if (deg > 22.5 & deg <= 67.5) degString = "Northeast ↗";
                    else if (deg > 67.5 & deg <= 112.5) degString = "North ⬆";
                    else if (deg > 112.5 & deg <= 157.5) degString = "Northwest ↖";
                    else if (deg > 157.5 & deg <= 202.5) degString = "West ⬅";
                    else if (deg > 202.5 & deg <= 247.5) degString = "Southwest ↙";
                    else if (deg > 247.5 & deg <= 292.5) degString = "South ⬇";
                    else if (deg > 292.5 & deg <= 337.5) degString = "Southeast ↘";

                    windTv.setText(wind.getString("speed") + " m/s " + degString + " (" + deg + "°)");
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WeatherActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(request);
    }

    @SuppressLint("MissingPermission")
    private void updatePosition() {
        Toast.makeText(this, "Searching position...", Toast.LENGTH_SHORT).show();
        fusedLocationClient.getCurrentLocation(PRIORITY_BALANCED_POWER_ACCURACY, new CancellationTokenSource().getToken())
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            lat = location.getLatitude();
                            lon = location.getLongitude();
                            Toast.makeText(WeatherActivity.this, "Position updated:\n" + lat + "," + lon, Toast.LENGTH_SHORT).show();
                            getWeather("https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon);
                        } else {
                            Toast.makeText(WeatherActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}